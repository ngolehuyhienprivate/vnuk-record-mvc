<%@ page import="java.util.*, vn.edu.vnuk.record.dao.ContactDao, vn.edu.vnuk.record.jdbc.model.Contact"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="bootstrap/bootstrap.css" type="text/css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="fontawesome/css/font-awesome.css">
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Read All Contacts</title>
	</head>
	<body>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
		<c:import url="../header.jsp"/>
		<h2> Recording contacts</h2>
		
		<table>
			<tr>
				<th>Actions</th>
				<th>Name</th>
				<th>Email</th>
				<th>Address</th>
				<th>Date of Birth</th>
			</tr>
			
			
			<c:forEach var="contact" items="${contacts}">
				<tr>
					<td>
						<a href="mvc?logic=contact.Delete&id=${contact.id}">
							<button class="btn btn-danger" title="Delete this contact!">
							<i class="fa fa-times" aria-hidden="true">
							</i>
							</button>
						</a>
						<a href="mvc?logic=contact.Show&id=${contact.id}">
							<button class="btn btn-default" title="Show this contact!">
								<i class="fa fa-eye" aria-hidden="true"></i>
							</button>
						</a>
					</td>
					<td>${contact.name}</td>
					<td>
					<c:choose>
					<c:when test="${not empty contact.email}">
						<a href="mailto:${contact.email}">${contact.email}</a>
					</c:when>
					
					<c:otherwise>
						<i>No email address</i>
					</c:otherwise>
					</c:choose>
					</td>
					<td>${contact.address}</td>
					<td><fmt:formatDate value="${contact.dateOfBirth.time}" pattern="dd/MM/yyyy"/></td>
						
				</tr>
			</c:forEach>
			
		</table>
		
		<br/>
		
		<a href="mvc?logic=contact.Create">
			<button class="btn btn-primary" title="Create a new contact!">Create a new Contact</button>
		</a>
		
		<a href="http://localhost:8080/vnuk-record-mvc/">
			<button class="btn btn-default" title="Back to the menu page!">Back to menu</button>
		</a>
		
		<c:import url="../footer.jsp"/>
	</body>
</html>