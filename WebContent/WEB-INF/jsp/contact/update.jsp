<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="bootstrap/bootstrap.css" type="text/css" rel="stylesheet">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-ui.js"></script>
		<link rel="stylesheet" href="css/jquery-ui.css">
		
		<title>Update a contact</title>
	</head>
	<body>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
		<%@ taglib tagdir="/WEB-INF/tags" prefix="vnuk"%>
		
		<c:import url="../header.jsp"/>
		
		<h1>Update a contact</h1>
		<hr/>
		
		<form action="updateContact" method ="POST">
			<input type="hidden" name="id" value="${contact.id}">
			Name: <input type="text" name="name" value="${contact.name}"/><br/>
			Email: <input type="text" name="email" value="${contact.email}"/><br/>
			Address: <input type="text" name="address" value="${contact.address}"/><br/>
			Date of Birth: 
			<input type="text"
				id="date_of_birth"
				name="date_of_birth"
				value=<fmt:formatDate
					value="${contact.dateOfBirth.time}"
					pattern="dd/MM/yyyy"
				/>
			/>
			<br/>
			<input type="submit" value="Save" class="btn btn-success btn-xs"/>		
		</form>
		
		<br/>
		<a href="mvc?logic=contact.Index">
			<button class="btn btn-default" title="Back to the list of contacts!">Back</button>
		</a>
	
		<c:import url="../footer.jsp"/>
	</body>
</html>