<%@ page import="java.util.*, vn.edu.vnuk.record.dao.ContactDao, vn.edu.vnuk.record.jdbc.model.Contact"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="bootstrap/bootstrap.css" type="text/css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="fontawesome/css/font-awesome.css">
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Show a Contact</title>
	</head>
	<body>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
		<c:import url="../header.jsp"/>
		<h2> Show a contact</h2>
		
		<table>
			<tr>
				<th>Actions</th>
				<th>Name</th>
				<th>Email</th>
				<th>Address</th>
				<th>Date of Birth</th>
			</tr>
			
			<tr>
				<td>
					<a href="mvc?logic=contact.Delete&id=${contact.id}">
						<button class="btn btn-danger" title="Delete this contact!">
							<i class="fa fa-times" aria-hidden="true">
							</i>
						</button>
					</a>
				</td>
				<td>${contact.name}</td>
				<td>
				<c:choose>
				<c:when test="${not empty contact.email}">
					<a href="mailto:${contact.email}">${contact.email}</a>
				</c:when>
				
				<c:otherwise>
					<i>No email address</i>
				</c:otherwise>
				</c:choose>
				</td>
				<td>${contact.address}</td>
				<td><fmt:formatDate value="${contact.dateOfBirth.time}" pattern="dd/MM/yyyy"/></td>
					
			</tr>
			
		</table>
		
		<br/>
		<a href="mvc?logic=contact.Index">
			<button class="btn btn-default" title="Back to the list of contacts!">Back</button>
		</a>
		
		<a href="mvc?logic=contact.Update&id=${contact.id}">
			<button class="btn btn-success" title="Update this contact!">Update</button>
		</a>
		
		<c:import url="../footer.jsp"/>
	</body>
</html>