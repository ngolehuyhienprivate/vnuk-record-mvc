<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="bootstrap/bootstrap.css" type="text/css" rel="stylesheet">
		<script src="js/jquery.js"></script>
		<script src="js/jquery-ui.js"></script>
		<link rel="stylesheet" href="css/jquery-ui.css">
		
		<title>Add contact</title>
	</head>
	<body>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib tagdir="/WEB-INF/tags" prefix="vnuk"%>
		<c:import url="../header.jsp"/>
		
		<h1>Add a new contact</h1>
		<hr/>
		
		<form action="addContact" method ="POST">
			Name: <input type="text" name="name"/><br/>
			Email: <input type="text" name="email"/><br/>
			Address: <input type="text" name="address"/><br/>
			Date of birth:<vnuk:myDateField id = "date_of_birth"/><br/>
			<input type="submit" value="Save" class="btn btn-success btn-xs"/>
		</form>
		
		<br/>
		<a href="mvc?logic=contact.Index">
			<button class="btn btn-default btn-xs" title="Back to the list of contacts!">Back</button>
		</a>
		<c:import url="../footer.jsp"/>
	</body>
</html>