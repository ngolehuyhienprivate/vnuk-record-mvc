<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contact ${param.name} added</title>
    </head>
    <body>
        ${param.name} has successfully been added.
    </body>
</html>
