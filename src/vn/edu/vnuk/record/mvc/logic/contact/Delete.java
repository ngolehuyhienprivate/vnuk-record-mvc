package vn.edu.vnuk.record.mvc.logic.contact;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.edu.vnuk.record.dao.ContactDao;
import vn.edu.vnuk.record.mvc.logic.Logic;

public class Delete implements Logic {

	@Override
	public String run(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Long id = Long.parseLong(request.getParameter("id"));
		new ContactDao().delete(id);
		System.out.println("Deleting from contacts...");
		return "mvc?logic=contact.Index";
	}
	
}
