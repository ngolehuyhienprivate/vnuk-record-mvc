package vn.edu.vnuk.record.mvc.logic.contact;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.edu.vnuk.record.dao.ContactDao;
import vn.edu.vnuk.record.mvc.logic.Logic;

public class Show implements Logic{

	@Override
	public String run(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Long id = Long.parseLong(request.getParameter("id"));
		request.setAttribute("contact", new ContactDao().read(id));
		return "/WEB-INF/jsp/contact/show.jsp";
	}

}
